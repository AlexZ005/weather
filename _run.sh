#!/bin/bash
printf "[1] Build container and run"
printf "\n[2] Check for app changes and run locally"
printf "\n[3] Check for app changes and run locally in container"
printf "\n[0] Exit\n\n"

if [ ! -z "$1" ]
then
    REPLY=$1
else
    read -n1 -p "Enter:"
    echo
fi

function check_f() {
ext=".py"    
inotifywait -e close_write,moved_to,create -m ./app |
while read -r directory events filename; do
    if test "${filename#*$ext}" != "$filename"; then
        echo $(date +%H:%m:%S) "Executing changes"
        if [ "$1" == "local" ]; then
    	    python3 ./app/HoeWarmIsHetInDelft.py
    	fi
    	if [ "$1" == "container" ]; then
    	    echo "TODO: Building container"
    	fi
    fi
done
}

# commit project with changes
[[ $REPLY = [1] ]] && `./_build.sh`
# check for file changes in the app folder and start 
[[ $REPLY = [2] ]] && check_f "local"
# check for file changes in the app folder, build docker contariner and start
[[ $REPLY = [3] ]] && check_f "container"
